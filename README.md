# Ember

Ember is a fully-fledged framework for Spigot which enables you to rapidly develop powerful plugins for Minecraft 1.12.2 servers!

## Getting Started

### Prerequisites

You will need the following before using Ember.

* Your choice of IDE (IntelliJ, Eclipse, Notepad++, etcetera)
* JDK 8 installed on your development machine
* Intermediate/Advanced knowledge of the Bukkit API
* Any knowledge of the Spigot API
* A working knowledge of Maven
* The Maven Shade plugin configured for your project

### Installation

To installl Ember, simply include it as a dependency through Maven. You will need to have the Spigot API configured as a dependency as well.  

```xml

<dependency>
  <groupId>me.lukecarr</groupId>
  <artifactId>ember</artifactId>
  <version>0.1.0-alpha</version>
  <scope>compile</scope>
</dependency>

```

### Example Usage

Below you can find an example plugin class that utilises Ember to quickly define a command.  

```java

public final class Website extends JavaPlugin {

  EmberPlugin ember;
  
  public void onEnable() {
    ember = new EmberPlugin(this);
	new EmberCommand("website").ignoreArgs().allowFrom(Player.class).onCommand((sender) -> {
	  sender.sendMessage("Our website is https://somewebsite.here!");
	}).register(ember);
  }

}

```

This class creates a plugin called Website which contains one command (/website). This command does not require any arguments, so any passed arguments are ignored, and only command senders that are players can invoke the command. When the command is invoked, the message `"Our website is https://somewebsite.here!"` is sent. 

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Luke Carr (Tinkererr)** - *Project Lead and Initial Creator* - [Tinkererr](https://bitbucket.org/Tinkererr)

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE.md](LICENSE.md) file for details.
